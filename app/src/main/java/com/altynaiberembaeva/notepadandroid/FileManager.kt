package com.altynaiberembaeva.notepadandroid

import android.os.Environment
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

class FileManager {

    private val viewActivity: ViewActivity

    constructor(view: ViewActivity) {
        viewActivity = view
    }

    companion object {

        fun readFile(filePath: String, fileName: String): String {
            val file = File(filePath, fileName)
            val allLinesFile = StringBuilder()
            val bufferedReader = BufferedReader(FileReader(file))
            for (line in bufferedReader.readLine()) {
                allLinesFile.append(line)
            }
            bufferedReader.close()
            return allLinesFile.toString()
        }
    }

    fun saveFile(nameFile: String, textFile: String) {
        val filePath = File(Environment.getExternalStorageDirectory().path + "/Download")

        var check = true
        if (!filePath.exists())
            check = filePath.mkdir()

        if (check) {
            val file = File(nameFile + ".txt")

            val location = File(filePath.path, file.path)
            if (!location.exists())
                check = location.createNewFile()
            if (check) {

                try {
                    location.printWriter().use { out ->
                        out.println(textFile)
                    }

                } catch (e: Exception) {

                }
            }
        }
    }

    fun Delete(title: String) {
        val filePath =
            File(Environment.getExternalStorageDirectory().path + "/Download" + "/" + title + ".txt")
        filePath.delete()

    }
}



