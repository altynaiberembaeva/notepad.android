package com.altynaiberembaeva.notepadandroid

import android.app.AlertDialog
import android.content.Context
import android.graphics.Point
import android.os.Environment
import android.view.Display
import android.view.WindowManager
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import java.io.File

class OpenFileDialog : AlertDialog.Builder {

    private var fileDialogPath = Environment.getExternalStorageDirectory().path
    private val files: MutableList<File> = mutableListOf()
    private val linearLayout: LinearLayout
    private val viewActivity: ViewActivity

    constructor(view: ViewActivity, context: Context) : super(context) {
        viewActivity = view
        linearLayout = createMainLayout(context)
        setTitle(fileDialogPath)

        files.addAll(getFiles(fileDialogPath))
        val listView = createListView(context)
        listView!!.adapter = FileAdapter(this, context, files)
        setView(listView)
            .setNegativeButton(android.R.string.cancel, null)

    }

    private fun RebuildFiles(adapter: ArrayAdapter<File>) {
        setTitle(fileDialogPath)
        files.clear()
        files.addAll(getFiles(fileDialogPath))
        adapter.notifyDataSetChanged()
    }

    private fun createListView(context: Context): ListView? {
        val listView = ListView(context)
        listView.onItemClickListener =
            OnItemClickListener {
                    adapterView, view, index, l ->

                val adapter: ArrayAdapter<File> = adapterView.adapter as FileAdapter
                val file = adapter.getItem(index)
                println(file?.path)
                println(file?.absolutePath)
                if (file!!.isDirectory) { //является д
                    fileDialogPath = file.path
                    RebuildFiles(adapter)
                } else {
                    val contentOfFile = FileManager.readFile(file.parentFile.path, file.name)
                    viewActivity.editText.setText(contentOfFile, TextView.BufferType.EDITABLE)
                    viewActivity.title.setText(file.nameWithoutExtension)

                }

            }

        return listView
    }

    fun getFiles(directoryPath: String): Collection<File> {
        val directory = File(directoryPath)
        val files: List<File> = directory.listFiles().toList()

        return files
    }

    fun getDefaultDisplay(context: Context): Display? {
        return (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
    }

    fun getScreenSize(context: Context): Point {
        val screenSize = Point()
        getDefaultDisplay(context)?.getSize(screenSize)
        return screenSize

    }

    fun getLinearLayoutMinHeght(context: Context): Int {
        return getScreenSize(context).y
    }

    fun createMainLayout(context: Context): LinearLayout {
        var linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.minimumHeight = getLinearLayoutMinHeght(context)
        return linearLayout
    }

}










