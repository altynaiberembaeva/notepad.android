package com.altynaiberembaeva.notepadandroid

import android.view.MenuItem
import android.view.View
import android.widget.Toast
import java.lang.Exception

class Controller : View.OnClickListener {

    private val viewActivity: ViewActivity
    private val fileManager: FileManager

    constructor(view: ViewActivity) {
        viewActivity = view
        fileManager = FileManager(view)

    }

    override fun onClick(view: View) {

    }

    fun pullOutButtonsMenu(view: MenuItem) {
        when (view.itemId) {
            R.id.buttonOpen -> {
                val fileDialog = OpenFileDialog(viewActivity, viewActivity)
                fileDialog.show()
            }
            R.id.buttonNewFile -> {
                viewActivity.clearText()
            }
            R.id.buttonSave -> {
                saveText()
            }
            R.id.buttonDelete -> {
                fileManager.Delete(viewActivity.title.text.toString())
                viewActivity.clearText()
            }
        }
    }

    fun saveText() {

        var nameFile: String = viewActivity.title.text.toString()
        var textFile: String = viewActivity.editText.text.toString()

        try {
            fileManager.saveFile(nameFile, textFile)
            Toast.makeText(viewActivity, "File saved", Toast.LENGTH_SHORT).show()

        } catch (e: Exception) {
            Toast.makeText(viewActivity, "File not saved", Toast.LENGTH_SHORT).show()
        }
    }
}