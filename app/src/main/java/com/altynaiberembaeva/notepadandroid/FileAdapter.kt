package com.altynaiberembaeva.notepadandroid

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import java.io.File

class FileAdapter: ArrayAdapter<File> {

    private var openFileDialog: OpenFileDialog
    private var files: MutableList<File>

    constructor(openFileDialog: OpenFileDialog, context: Context, files: MutableList<File>)
            : super(context, android.R.layout.simple_list_item_1, files) {
        this.openFileDialog = openFileDialog
        this.files = files

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = super.getView(position, convertView, parent) as TextView
        val file = getItem(position)
        view.text = file?.name
        return view
    }

}







