package com.altynaiberembaeva.notepadandroid


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class ViewActivity : AppCompatActivity {

    private val controller: Controller
    lateinit var editText: EditText
    lateinit var title: EditText

    constructor() {
        controller = Controller(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        displayToolbar()
        checkRequestPermission()

        editText = findViewById(R.id.editText)
        title = findViewById(R.id.title)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true

    }

    fun clearText() {
        editText.text.clear()
        title.text.clear()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        controller.pullOutButtonsMenu(item)
        return true
    }

    fun displayToolbar() {
        setSupportActionBar(toolBar)

    }

    /**
     * Asking permission for external storage
     * - write;
     * - manage;
     * */
    private fun checkRequestPermission() {
        val permissionWrite = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
        if (permissionWrite != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
        //For Android Version R or higher, check manage external storage permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                val intent = Intent()
                intent.action = Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
                val uri: Uri = Uri.fromParts("package", this.packageName, null)
                intent.data = uri
                this.startActivity(intent)
            }
        }
    }

    /**
     * Calls when user allow or deny permission
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty()) {
                if (!(grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(this, R.string.message, Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }
    }
}



